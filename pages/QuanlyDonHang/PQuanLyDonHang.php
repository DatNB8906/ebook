
<html>
    <head>
        <style>

            .paging{
                font-size: 14px;
                text-align: right; 
                margin-top: 30px;
            }

            .paging a{
                color: #FFF;    
            }

            .page-item{
                /* border: 1px solid #ccc;
                padding: 5px 9px; */
                display: inline-block;
                width: 40px;
                height: 40px;
                line-height: 40px;
                text-align: center;
                background-color: #f0643b;
                -webkit-transition: 0.2s all;
                transition: 0.2s all;
                border-radius: 2px;
            }

            .paging a:hover{
                background-color: #3B434E;
                color: #D10024;
            }

            .paging a:active{
                background-color: #D10024;
                border-color: #D10024;
                color: #FFF;
                font-weight: 500;
                cursor: default;
            } 
        </style>
    </head>
</html>
            <div class="content-page">
                <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <!-- <div class="row">
                        <h4 class="page-title">Hiển thị đơn hàng</h4>
                        </div>      -->
                        <!-- end page title --> 
                        
                        <table>
                            <tr style="text-align: center; font-size: 15px; ">
                                <th width = "150" style="text-align: center;">Tên sản phẩm</th>
                                <th width = "150" style="text-align: center;">Hình ảnh</th>
                                <th width = "120" style="text-align: center;">Số lượng</th>
                                <th width = "120" style="text-align: center;">Ngày mua</th>
                                <th width = "180" style="text-align: center;">Tổng tiền</th>
                            </tr>
                            <?php
                                $id = $_SESSION["idUser"];
                                $url = 'index.php';
                                $sql= "SELECT * FROM `purchase` WHERE `User_Id`  = $id";
                                $purchaseId = DataProvider::ExecuteQuery($sql);   
                                if(mysqli_num_rows($purchaseId) == 0){
                                    echo "<script type='text/javascript'>alert('Bạn không có đơn hàng nào');</script>";
            	                    DataProvider::ChangeURL($url);
                                }
                                
                                while($rowpush = mysqli_fetch_array($purchaseId)){
                                $idPush = $rowpush["Purchase_Id"];
                                $sqlpus= "SELECT * FROM `purchasedetail` WHERE `Purchase_Id`  = $idPush";
                                $purchase = DataProvider::ExecuteQuery($sqlpus); 
                                while($row = mysqli_fetch_array($purchase))
                                {
                                    $idPro = $row["Product_Id"];
                                    $sqlpage= "SELECT * FROM `product` where `Product_Id` = $idPro";
                                    $products = DataProvider::ExecuteQuery($sqlpage);
                                    $rowPro = mysqli_fetch_array($products);
                                    ?>
                                        <tr style="text-align: center;"> 
                                            <td><?php echo $rowPro["Name"]; ?></td>
                                            <td><img src="<?= $rowPro["Avatar"] ?>" style="width:70px; height: 70px; margin-top: 15px" /></td>
                                            <td><?php echo $row["Quantity"]; ?></td>
                                            <td><?php echo $rowpush["CreatedAt"]; ?></td>
                                            <td><?php echo $row["TotalAmount"]; ?></td>
                                        </tr>
                                    <?php
                                }
                            }
                            ?>	
                        </table>                          
                    </div> <!-- container -->

                </div> <!-- content -->
            </div>
