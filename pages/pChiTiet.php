<?php
	include './pages-handle/xlChiTietSP.php';
	//echo $result[0]['Avatar'];
?>

<!-- Chi tiết sản phẩm -->
<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- Product main img -->
					<div class="col-md-5 col-md-push-2">
						<div id="product-main-img">

							<div class="product-preview">
								<img src="<?php echo $result[0]['Avatar'] ?>" alt="">
							</div>

							<div class="product-preview">
								<img src="<?php echo $result[0]['Avatar'] ?>" alt="">
							</div>	

						</div>
					</div>
					<!-- /Product main img -->

					<!-- Product thumb imgs -->
					<div class="col-md-2  col-md-pull-5">
						<div id="product-imgs">

							<div class="product-preview">
								<img src="<?php echo $result[0]['Avatar'] ?>" alt="">
							</div>

							<div class="product-preview">
								<img src="<?php echo $result[0]['Avatar'] ?>" alt="">
							</div>

						</div>
					</div>
					<!-- /Product thumb imgs -->

					<!-- Product details -->
					<div class="col-md-5">
						<div class="product-details">
							<h2 class="product-name"><?php echo $result[0]['Name'] ?></h2>
							<p class="product-category"><?php echo $result[0]['Publishing_Company_Name'] ?></p>
							<div>
								<div class="product-rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
								</div>
							</div>
							<div>
								<h3 class="product-price"><?php echo number_format($result[0]['Price'], 0, '.', '.'). ' ₫' ?></h3>								
							</div>

							<p><?php echo $result[0]['Description'] ?></p>							

							<div class="add-to-cart">
								<div class="qty-label">
									Số lượng
									<div class="input-number">
										<input type="number" id="slSanPham" value="1">
										<span class="qty-up">+</span>
										<span class="qty-down">-</span>
									</div>
								</div>
								<?php 
									if($result[0]['Quantity'] > 0) 
										echo '<button class="add-to-cart-btn" onclick="cartAction(\'add\', \'' . $result[0]['Product_Id'] . '\')"><i class="fa fa-shopping-cart"></i>Thêm vào giỏ hàng</button>';
									else
										echo '<button class="add-to-cart-btn-disable" disabled>Hết hàng</button>';
								?>
							</div>

							<ul class="product-links">
								<li>Category:</li>
								<li><a href="<?php echo 'index.php?page=SanPham&view=search&q=' . $result[0]['Category_Name'] ?>"><?php echo $result[0]['Category_Name'] ?></a></li>
							</ul>

						</div>
					</div>
					<!-- /Product details -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
</div>
<!-- /Chi tiết sản phẩm -->

<!-- Sản phẩm xem gần đây -->

<!-- /Sản phẩm xem gần đây -->