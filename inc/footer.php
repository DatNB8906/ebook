<!-- FOOTER -->
<footer id="footer">
			<!-- top footer -->
			<div class="section">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<div class="col-md-4 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">Về chúng tôi</h3>
								<ul class="footer-links">
									<li><a href="#"><i class="fa fa-map-marker"></i>Học viện kĩ thuật mật mã</a></li>
									<li><a href="#"><i class="fa fa-phone"></i>0123456789</a></li>
									<li><a href="#"><i class="fa fa-envelope-o"></i>admin@ebook.com</a></li>
								</ul>
							</div>
						</div>

						
					</div>
					<!-- /row -->
				</div>
				<!-- /container -->
			</div>
			<!-- /top footer -->
			
		</footer>
		<!-- /FOOTER -->
